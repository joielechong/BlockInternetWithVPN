package com.rilixtech.blockinternetwithvpn;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.VpnService;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

  private boolean mIsServiceRunning = false;
  private Button mBtnBlock;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    mBtnBlock = findViewById(R.id.main_block_btn);
    mBtnBlock.setOnClickListener(new View.OnClickListener() {
      @Override public void onClick(View v) {
        if (mIsServiceRunning) {
          stopBlockerVpnService();
        } else {
          startBlockerVpnService();
        }
      }
    });
  }

  private BroadcastReceiver pongReceiver = new BroadcastReceiver() {
    @Override public void onReceive(Context context, Intent intent) {
      mIsServiceRunning = true;
      Toast.makeText(context, "VPN CONNECTED", Toast.LENGTH_LONG).show();
      mBtnBlock.setText("Unblock Internet");
    }
  };

  private void startBlockerVpnService() {
    BlockerWithVpnService.registerReceiver(this, pongReceiver);
    Intent intent = VpnService.prepare(getApplicationContext());
    if (null == intent) {
      onActivityResult(0, RESULT_OK, null);
    } else {
      startActivityForResult(intent, 0);
    }
  }

  private void stopBlockerVpnService() {
    BlockerWithVpnService.unRegisterReceiver(this, pongReceiver);
    BlockerWithVpnService.stopMySelf(this);
    mIsServiceRunning = false;
    Toast.makeText(this, "VPN DISCONNECTED", Toast.LENGTH_LONG).show();
    mBtnBlock.setText("Block Internet");
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    if (resultCode == RESULT_OK) {
      BlockerWithVpnService.startMySelf(this);
    }
  }
}
