package com.rilixtech.blockinternetwithvpn;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.VpnService;
import android.os.ParcelFileDescriptor;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.channels.DatagramChannel;

public class BlockerWithVpnService extends VpnService {
  public static final String TAG = BlockerWithVpnService.class.getSimpleName();
  public static final String HOST_NAME = "127.0.0.1";
  public static final int PORT = 8087;
  public static final long SLEEP_TIME = 60000; // One minutes in millis.

  private static DatagramSocketThread mDatagramSocketThread;
  private static final String STOP_EXTRA = "stopExtra";

  private Thread mThread;
  private ParcelFileDescriptor mInterface;
  private Builder builder = new Builder();

  private boolean mKeepAlive = true;
  private DatagramChannel tunnel;

  @Override
  public void onCreate() {
    LocalBroadcastManager
        .getInstance(this)
        .registerReceiver(pingReceiver, new IntentFilter(PING_FILTER));
  }

  @Override
  public int onStartCommand(Intent intent, int flags, int startId) {
    mThread = new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          // Configure the TUN and get the interface.
          // use IPv4 Address Blocks Reserved for Documentation
          // see https://tools.ietf.org/html/rfc5737
          mInterface = builder.setSession("BlockerWithVpnService")
              .addAddress("203.0.113.1", 24)
              .addDnsServer("203.0.113.0")
              .addRoute("0.0.0.0", 0)
              .establish();

          tunnel = DatagramChannel.open();
          tunnel.connect(new InetSocketAddress(HOST_NAME, PORT));
          Log.e("Localhost: ", InetAddress.getLocalHost().toString());

          // Protect the socket so the package sent by it won't be feedback to the vpn service.
          protect(tunnel.socket());
          while (mKeepAlive) Thread.sleep(SLEEP_TIME);
        } catch (Exception e) {
          e.printStackTrace();
        } finally {
          try {
            if (mInterface != null) {
              mInterface.close();
              mInterface = null;
            }
          } catch (Exception e) {
            Log.e(TAG, "Error : " + e.toString());
          }
        }
      }
    }, "BlockerWithVpnServiceRunnable");
    mThread.start();

    LocalBroadcastManager
        .getInstance(BlockerWithVpnService.this)
        .sendBroadcastSync(new Intent(PONG_FILTER));

    return START_STICKY;
  }

  @Override
  public void onDestroy() {
    Log.d(TAG, "onDestroy() called");
    if (mThread != null) {
      mThread.interrupt();
    }
    try {
      if (tunnel != null) tunnel.close();
      if (mInterface != null) {
        mInterface.close();
        mInterface = null;
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    LocalBroadcastManager.getInstance(this).unregisterReceiver(pingReceiver);
    super.onDestroy();
  }

  public static final String PING_FILTER = "ping";
  public static final String PONG_FILTER = "pong";

  private BroadcastReceiver pingReceiver = new BroadcastReceiver() {
    @Override public void onReceive(Context context, Intent intent) {
      boolean isStop = intent.getBooleanExtra(STOP_EXTRA, false);
      Log.d(TAG, "isStop = " + isStop);
      if (isStop) {
        stopMySelf();
        return;
      }

      Log.d(TAG, "Ping received");
      LocalBroadcastManager
          .getInstance(BlockerWithVpnService.this)
          .sendBroadcastSync(new Intent(PONG_FILTER));
    }
  };

  public void stopMySelf() {
    Log.d(TAG, "stopMySelf called");
    mKeepAlive = false;
    if (mDatagramSocketThread != null) mDatagramSocketThread.stopThread();
    mThread.interrupt();
    stopSelf();
  }

  public static void stopMySelf(Context context) {
    Intent intent = new Intent(context, BlockerWithVpnService.class);
    Intent intentFilter = new Intent(PING_FILTER);
    intentFilter.putExtra(STOP_EXTRA, true);
    LocalBroadcastManager.getInstance(context).sendBroadcastSync(intentFilter);
    context.stopService(intent);
  }

  public static void startMySelf(Context context) {
    mDatagramSocketThread = new DatagramSocketThread();
    mDatagramSocketThread.start();
    context.startService(new Intent(context, BlockerWithVpnService.class));
  }

  public static void registerReceiver(Context context, BroadcastReceiver receiver) {
    LocalBroadcastManager
        .getInstance(context)
        .registerReceiver(receiver, new IntentFilter(PONG_FILTER));
  }

  public static void unRegisterReceiver(Context context, BroadcastReceiver receiver) {
    LocalBroadcastManager
        .getInstance(context)
        .unregisterReceiver(receiver);

  }
}