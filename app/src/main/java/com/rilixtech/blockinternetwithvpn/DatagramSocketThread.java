package com.rilixtech.blockinternetwithvpn;

import android.util.Log;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;

public class DatagramSocketThread extends Thread {
  public static final String TAG = DatagramSocketThread.class.getSimpleName();

  private DatagramSocket mDatagramSocket;
  private boolean mIsSocketAlive = true;

  @Override
  public void run() {
    try {
      mDatagramSocket = new DatagramSocket(
          new InetSocketAddress(BlockerWithVpnService.HOST_NAME, BlockerWithVpnService.PORT));
      byte[] buffer = new byte[32767];
      DatagramPacket datagramPacket = new DatagramPacket(buffer, buffer.length);
      while (mIsSocketAlive) {
        datagramPacket.setLength(buffer.length);
        mDatagramSocket.receive(datagramPacket);
        Log.d(TAG, "receive packet");
        Thread.sleep(10000);
      }
    } catch (SocketException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public void stopThread() {
    mIsSocketAlive = false;
    if (mDatagramSocket != null) mDatagramSocket.close();
    interrupt();
  }
}
